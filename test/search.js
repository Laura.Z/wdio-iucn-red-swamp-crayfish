describe('iucn search red swamp crayfish', () => {

    it('should open main url', async () => {
        browser.maximizeWindow();
        await browser.url('https://www.iucnredlist.org');
    })

    it('should click search bar', async () => {
        const searchBar = await $('.search');
        await searchBar.click();
    })

    it('should type red swamp crayfish', async () => {
        const input = await $('.search');
        await input.setValue('red swamp crayfish');
    })

    it('should click site button key', async () => {
        const siteButtonKey = await $('//span[@class="search--site__button__key"]');
        await siteButtonKey.click();
    })

    it('should click red swamp crayfish box', async () => {
        const box = await $('//*[@id="redlist-js"]/div/div/div[2]/section/div[2]/article[1]/a');
        await box.click();
        await browser.pause(7000);


    })


})